#需要注意
#1.使用本地菜单的话要在 router文件夹里的menu.js里添加，请仿照给的例子，下面的例子是一个完整的三层结构
{
        path: '/activity',//这是最外层的路由
        name: 'activity',//这个name最好和路由一样
        title: '综合配置',
        desc: 'language.menu.sysNot',
        children: [
            {
                path: '/activity',//所有的页面都会继承一个空页面，所以要写这一层
                name: 'sysNot',//这个name不能和其他的name一样，随便起倒是没啥问题
                title: '配置',
                desc: 'language.menu.sysNot',
                children: [
                    {
                        path: '/activity/detail',
                        name: 'detail',
                        title: '活动管理',
                        desc: '',
                        children: []//这个children要有，因为登录获取第一个地址的时候会判断length
                    }
                ]
            }]
}       
#2.router里的路由配置要和 menu.js的一样 但是不需要有中间那一层
#3.正常的表格生成详见views文件夹的activity的about.vue
#4.common文件夹里的httpUtils里面有http请求。正常使用 就post的方法this.$http(url,{参数}) get方法：this.$get(url,{参数})
#5.一个完整的增删改查详见shop文件夹。
#6.directive里有两个自定义指令 v-has是按钮级别权限。使用方法是 在需要加权限的按钮的标签上添加 v-has="['edit:index']"   还有一个是弹框可以拖动的指令在弹框的标签上添加v-dialogDrag
