import { urlConfig } from './config/UrlConfig';


const baseURL = `${urlConfig.baseIp}`;


//console.log(baseURL,'baseUrl')
/**
 * 拼接请求的url
 */

let urlUtils = {}

//首页
urlUtils.GetNewNavList = `${baseURL}/web/news/index`;  // 首页新闻资讯导航

//新闻
urlUtils.GetNewDetail = `${baseURL}/web/news/detail`;  // 新闻详情
urlUtils.GetNewList = `${baseURL}/web/news/list`;  // 新闻列表

//拍卖会
urlUtils.GetMeetList = `${baseURL}/web/meet/list`;  // 前台首页拍卖会列表
urlUtils.GetMeetDetail = `${baseURL}/web/meet/detail`;  // 拍卖会详情
urlUtils.GetMeetNoticeDetail = `${baseURL}/web/meet/noticeDetail`;  // 公告详情

//标的
urlUtils.GetlotList = `${baseURL}/web/lot/list`;  // 拍卖会详情 - 标的列表

//拍卖公告详情
urlUtils.GetNoticeDetail = `${baseURL}/web/notice/detail`;  // 拍卖会详情 - 拍卖公告

//公告
urlUtils.GetNoticeList = `${baseURL}/web/notice/list`;  // 拍卖会详情 - 公告列表

//用户
urlUtils.UserRegister = `${baseURL}/web/usersInfo/register`;  // 注册
urlUtils.UserLogin = `${baseURL}/web/usersInfo/login`;  // 登录
urlUtils.UserCheckInfo = `${baseURL}/web/usersInfo/checkUsername`;//根据用户名获取用户信息
urlUtils.UserForgetPassword = `${baseURL}/web/usersInfo/forgetPassword`;//重置密码
urlUtils.UserInfoGet = `${baseURL}/web/userCenter/usersInfoDetail`;//个人信息
urlUtils.UserReal = `${baseURL}/web/userCenter/real`;//实名
urlUtils.UserSignUpEd = `${baseURL}/web/userCenter/mySignUp`;//已报名的拍卖会
urlUtils.UserBuyEd = `${baseURL}/web/userCenter/myBid`;//已拍下的拍卖会
urlUtils.UserBuyEdInfo = `${baseURL}/web/userCenter/checkPay`;//获取买受人信息
urlUtils.UserPay = `${baseURL}/web/userCenter/Pay`;//付款接口

//报名
urlUtils.UserCheckAndInfo = `${baseURL}/web/bidder/webCheckAndInfo`;//点击报名按钮进行相关验证并返回信息
urlUtils.UserSignUp = `${baseURL}/web/bidder/signUp`;//报名按钮

//拍卖大厅
urlUtils.GetAllLot = `${baseURL}/web/lot/all/list`;  // 获取拍卖会下的所有标的
urlUtils.GetBidList = `${baseURL}/web/bidder/bidList`;  // 获取竞价记录
urlUtils.Bidding = `${baseURL}/web/bidder/bidding`;  // 出价
urlUtils.CheckSignUp = `${baseURL}/web/bidder/webCheckSignUp`;  // 竞买人是否报名拍卖会

//图片上传
urlUtils.FileUpload = `${baseURL}/sys/upload`;  // 图片上传接口

//个人中心
//urlUtils.UsersInfoDetail = `${baseURL}/web/userCenter/real`;  // 分页查询竞买人列表


export {
    urlUtils,
    baseURL
}
