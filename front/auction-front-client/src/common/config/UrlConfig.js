
/**
 * 配置网络请求环境
 * @type {string}
 */

let env = process.env.VUE_APP_CURRENTMODE || 'DEV';

let GlobalConfig = {
    DEV: {
        env: 'DEV',
        baseIp:'/api'
    },
}

let urlConfig = GlobalConfig[env];
export {
    urlConfig,
    env
}
