import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/baseLayout'
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/login'),
    meta: {
      title: '网上拍卖平台'
    }
  },
  {
    path: '/register',
    name: 'register',
    component: () => import('../views/user/register'),
    meta: {
      title: '网上拍卖平台'
    }
  },
  {
    path: '/forgetpass',
    name: 'forgetpass',
    component: () => import('../views/user/forgetpass'),
    meta: {
      title: '网上拍卖平台'
    }
  },
  {
    path: '/updatepass',
    name: 'updatepass',
    component: () => import('../views/user/updatepass'),
    meta: {
      title: '网上拍卖平台'
    }
  },
  {
    path: '/verification',
    name: 'verification',
    component: () => import('../views/user/verification'),
    meta: {
      title: '网上拍卖平台'
    }
  },{
    path: '/usercenter',
    name: 'usercenter',
    component: () => import('../views/user/usercenter'),
    meta: {
      title: '网上拍卖平台'
    }
  },{
    path: '/realname',
    name: 'realname',
    component: () => import('../views/user/realname'),
    meta: {
      title: '网上拍卖平台'
    }
  },
  {
    path: '/register',
    name: 'register',
    component: () => import('../views/user/register'),
    meta: {
      title: '网上拍卖平台'
    }
  },
  {
    path: '/baseLayout',
    name: 'baseLayout',
    component: () => import('../components/layout/baseLayout'),
    meta: {
      subMenu: true, //是否含有子菜单，是否在头部展示tab
      title: '网上拍卖平台',
      desc: ''
    },
  },{
    path: '/newsDetail',
    name: 'newsDetail',
    component: () => import('../views/news/newsDetail.vue'),
    meta: {
      title: '网上拍卖平台'
    }
  },{
    path: '/newsList',
    name: 'newsList',
    component: () => import('../views/news/newsList.vue'),
    meta: {
      title: '网上拍卖平台'
    }
  },{
    path: '/meetDetail',
    name: 'meetDetail',
    component: () => import('../views/meet/meetDetail.vue'),
    meta: {
      title: '网上拍卖平台'
    }
  },{
    path: '/NoticeDetailPage',
    name: 'NoticeDetailPage',
    component: () => import('../views/home/NoticeDetailPage.vue'),
    meta: {
      title: '网上拍卖平台'
    }
  },{
    path: '/about',
    name: 'About',
    component: () => import('../views/home/About.vue'),
    meta: {
      title: '网上拍卖平台'
    }
  },{
    path: '/auctionhall',
    name: 'auctionhall',
    component: () => import('../views/auction/auctionhall.vue'),
    meta: {
      title: '网上拍卖平台'
    }
  }
  ,{
    path: '/signup',
    name: 'signup',
    component: () => import('../views/user/signup.vue'),
    meta: {
      title: '网上拍卖平台'
    }
  }
  ,{
    path: '/paypage',
    name: 'paypage',
    component: () => import('../views/user/paypage.vue'),
    meta: {
      title: '网上拍卖平台'
    }
  }
]

const router = new VueRouter({
  routes
})

export default router
