import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        loadingShow: false,
        toastObj: {
            showToast: false,
            msg: '',
            duration: 2000
        },
        currentPageName: '',
        currentTab: '',  // 当前一级菜单
        currentMenu: '',  // 当前三级菜单
        breadcrumbList: [],  // 面包屑数据
    },
    mutations: {
        setToastObj(state, toastObj) {
            if (!toastObj.showToast) {
                toastObj.showToast = false
            }
            if (!toastObj.msg) {
                toastObj.msg = ''
            }
            if (!toastObj.duration || isNaN(toastObj.duration) || toastObj.duration < 500) {
                toastObj.duration = 2000
            }
            state.toastObj = toastObj
        },
        setPageName(state, page) {
            state.currentPageName = page
        },
        setCurrentTab(state, tab) {
            state.currentTab = tab;
        },
        setCurrentMenu(state, menu) {
            state.currentMenu = menu
        },
    },
    actions: {},
    modules: {},
    getters: {
        getLoadingState(state) {
            return state.loadingShow
        },
        getToastObj(state) {
            return state.toastObj
        },
        getBreadcrumbList(state) {
            return state.breadcrumbList
        },
    }
})
