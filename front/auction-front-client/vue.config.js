/*
 * @Author: your name
 * @Date: 2020-03-23 19:28:32
 * @LastEditTime: 2020-04-21 10:54:34
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: \etrain-webpc\vue.config.js
 */
// const CompressionPlugin = require('compression-webpack-plugin')
// const Timestamp = new Date().getTime();
module.exports = {
    lintOnSave: false,  // 是否开启eslint
    publicPath: './',    // 打包文件相对路径地址
    outputDir: 'east',  // 打包后文件夹名称
    devServer: {
        open: true,//是否自动弹出
        port: 8092,//端口号
        https: false,
        hotOnly: true,//热更新
        proxy: {
            '/api': {
                target: 'http://localhost:8081/',//目标地址
                // ws: true, // 是否启用websockets
                secure: false, // 使用的是http协议则设置为false，https协议则设置为true
                changeOrigin: true, //开启代理：在本地会创建一个虚拟服务端，然后发送请求的数据，并同时接收请求的数据，这样服务端和服务端进行数据的交互就不会有跨域问题
                pathRewrite: { '^/api': '' }    //这里重写路径
            }
        }

    },
}