import Vue from 'vue'
import axios from 'axios'
var V = new Vue();
import { urlUtils, baseURL } from './UrlUtils'
// import store from '../store'

import { commonUtils } from './util'
import { ColorPicker } from 'element-ui';

let temp = 0

/**
 * 拦截请求数据，在请求开始的时候展示loading
 **/

axios.defaults.baseURL = '';
// axios.defaults.withCredentials = true;
axios.interceptors.request.use(function (config) {
    // config.headers["Authentication"] = commonUtils.getCookie('token');
    return config
}, function (error) {
    return Promise.reject(error)
})


let that = this;

/**
 * 拦截响应，在响应结束的时候关闭loading
 */
axios.interceptors.response.use(function (response) {
    //console.log(response.headers,'response')
    return response
}, function (err) {
    if (err && err.response) {
        switch (err.response.status) {
            case 400:
                err.message = '请求错误(400)';
                break
            case 401:
                err.message = '未授权，请重新登录(401)';
                break
            case 403:
                err.message = '拒绝访问(403)';
                break
            case 404:
                err.message = '请求出错(404)';
                V.$message.error('身份已过期,请重新登录')
                break
            case 408:
                err.message = '请求超时(408)';
                break
            case 500:
                err.message = '服务器错误(500)';
                break
            case 501:
                err.message = '服务未实现(501)';
                break
            case 502:
                err.message = '网络错误(502)';
                break
            case 503:
                err.message = '服务不可用(503)';
                break
            case 504:
                err.message = '网络超时(504)';
                break
            case 505:
                err.message = 'HTTP版本不受支持(505)';
                break
            default:
                err.message = `连接出错(${err.response.status})!`
        }
    } else {
        if (err && err.message.includes('timeout')) {   // 判断请求异常信息中是否含有超时timeout字符串
            err.message = '网络连接超时'          // reject这个错误信息
        } else {
            err.message = '连接服务器失败!'
        }
    }
    return Promise.reject(err)
})


/**
 * 暴露请求方法
 * url 请求的链接
 * data 请求的数据
 * loading 是否展示loading
 * header 请求头
 */
export default {
    install(Vue, options) {
        Vue.prototype.$http = (url, data, loading = true, header = {}) => {
            if (url.indexOf('http') === -1 && !url.includes('/api')) {
                url = urlUtils[url]
            }
            // //console.log('请求参数 :', url, data)
            return new Promise((resolve, reject) => {
                axios({
                    method: 'post',
                    url: url || '',
                    headers: Object.assign({
                        timeout: 60 * 1000 * 200,
                    }, header),
                    timeout: 60 * 1000 * 200,
                    'Content-Type': 'application/json',
                    //'Content-Type': 'multipart/form-data',
                    data: data,
                    loading: loading
                }).then((data) => {
                    //console.log('返回',data)
                    if (data.status && data.status === 200) {
                        resolve(data.data)
                    } else {
                        reject('网络请求错误')
                    }
                }, (e) => {
                    reject(e.message)
                }).catch((err) => {
                    reject('网络请求错误')
                })
            })
        }
        //删除
        Vue.prototype.$delete = (url, data, loading = true, header = {}) => {
            if (url.indexOf('http') === -1 && !url.includes('/api')) {
                url = urlUtils[url] + data
            }
            console.log('请求参数 :', url)
            return new Promise((resolve, reject) => {
                axios({
                    method: 'delete',
                    url: url || '',
                    headers: Object.assign({
                        timeout: 60 * 1000 * 200,
                    }, header),
                    timeout: 60 * 1000 * 200,
                    'Content-Type': 'application/json',
                    //'Content-Type': 'multipart/form-data',
                    data: null,
                    loading: loading
                }).then((data) => {
                    //console.log('返回',data)
                    if (data.status && data.status === 200) {
                        resolve(data.data)
                    } else {
                        reject('网络请求错误')
                    }
                }, (e) => {
                    reject(e.message)
                }).catch((err) => {
                    reject('网络请求错误')
                })
            })
        }
        //编辑
        Vue.prototype.$put = (url, data, loading = true, header = {}) => {
            if (url.indexOf('http') === -1 && !url.includes('/api')) {
                url = urlUtils[url]
            }
            // //console.log('请求参数 :', url, data)
            return new Promise((resolve, reject) => {
                axios({
                    method: 'put',
                    url: url || '',
                    headers: Object.assign({
                        timeout: 60 * 1000 * 200,
                    }, header),
                    timeout: 60 * 1000 * 200,
                    'Content-Type': 'application/json',
                    //'Content-Type': 'multipart/form-data',
                    data: data,
                    loading: loading
                }).then((data) => {
                    //console.log('返回',data)
                    if (data.status && data.status === 200) {
                        resolve(data.data)
                    } else {
                        reject('网络请求错误')
                    }
                }, (e) => {
                    reject(e.message)
                }).catch((err) => {
                    reject('网络请求错误')
                })
            })
        }
        //上传
        Vue.prototype.$formData = (url, data, loading = true, header = {}) => {
            if (url.indexOf('http') === -1 && !url.includes('/api')) {
                url = urlUtils[url]
            }
            return new Promise((resolve, reject) => {
                axios({
                    method: 'post',
                    url: url || '',
                    headers: Object.assign({
                        timeout: 60 * 1000 * 200,
                        'Content-Type': 'multipart/form-data;charset=UTF-8',
                        "Accept": "*/*",
                         loading: loading
                    }, header),
                    data: data,
                    'loading': loading
                }).then((data) => {
                    if (data.status && data.status === 200) {
                        resolve(data.data)
                    } else {
                        reject('网络请求错误')
                    }
                }, (e) => {
                    reject(e.message)
                }).catch((err) => {
                    reject('网络请求错误')
                })
            })
        }
        //post 下载 文件
        Vue.prototype.$post = (url, data, loading = true, header = {}) => {
            if (url.indexOf('http') === -1 && !url.includes('/api')) {
                url = urlUtils[url]
            }
            return new Promise((resolve, reject) => {

                axios({
                    method: 'post',
                    url: url || '',
                    headers: Object.assign({
                        timeout: 60 * 1000 *200,
                        loading: loading,

                    }, header),
                    // 'Content-Type': 'application/json',
                    responseType: "blob",
                    data: data,
                    'loading': loading
                }).then((data) => {
                    if (data.status && data.status === 200) {
                        resolve(data.data)
                    } else {
                        reject(err)
                    }
                }, (e) => {
                    reject(e.message)
                }).catch((err) => {
                    reject(err)
                })
            })
        }
        Vue.prototype.$get = (url, data, loading = true, header = {}) => {
            if (url.indexOf('http') === -1) {
                url = urlUtils[url]
            }
            return new Promise((resolve, reject) => {
                axios({
                    method: 'get',
                    url: url || '',
                    headers: Object.assign({
                        timeout: 60 * 1000 * 200,
                        //'Content-Type': 'application/json',
                    }, header),
                    timeout: 60 * 1000 * 200,
                    //'Content-Type': 'application/json',
                    params: data,
                    loading: loading
                }).then((data) => {
                    if (data.status && data.status === 200) {
                        resolve(data.data)
                    } else {
                        reject('网络请求错误')
                    }
                }, (e) => {
                    reject(e.message)
                }).catch((err) => {
                    reject('网络请求错误')
                })
            })
        }
        //get下载文件
        Vue.prototype.$getWith = (url, data, loading = true, header = {}) => {
            if (url.indexOf('http') === -1) {
                url = urlUtils[url]
            }

            return new Promise((resolve, reject) => {
                axios({
                    method: 'get',
                    url: url || '',
                    headers: Object.assign({
                        timeout: 60 * 1000 * 200,
                    }, header),
                    timeout: 60 * 1000 * 200,
                    // 'Content-Type': 'application/zip',
                    responseType: "blob",
                    params: data,
                    loading: loading
                }).then((data) => {
                    if (data.status && data.status === 200) {
                        resolve(data.data)
                    } else {
                        reject('网络请求错误')
                    }
                }, (e) => {
                    reject(e.message)
                }).catch((err) => {
                    reject('网络请求错误')
                })
            })
        }
    }
}

