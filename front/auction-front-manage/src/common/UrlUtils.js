import { urlConfig } from './config/UrlConfig';


const baseURL = `${urlConfig.baseIp}`;


//console.log(baseURL,'baseUrl')
/**
 * 拼接请求的url
 */

let urlUtils = {}

//后台登录
urlUtils.UserLogin = `${baseURL}/sys/login`; //用户登录
urlUtils.UpdatePassword= `${baseURL}/web/usersInfo/forgetPassword`;//修改密码

//用户管理
urlUtils.GetUserListByPage = `${baseURL}/sys/userManagement/list`;  // 分页查询用户列表
urlUtils.UserAdd = `${baseURL}/sys/userManagement/add`;  // 新增用户
urlUtils.UserUpdate = `${baseURL}/sys/userManagement/modify`;  // 修改用户
urlUtils.UserDelete = `${baseURL}/sys/userManagement/delete/`;  // 删除用户
urlUtils.UserResetPas= `${baseURL}/sys/userManagement/reset`;//后台用户修改密码

//新闻管理
urlUtils.GetNewsListByPage = `${baseURL}/sys/news/list`;  // 分页查询新闻列表
urlUtils.NewsAdd = `${baseURL}/sys/news/add`;  //新增新闻
urlUtils.NewsModify = `${baseURL}/sys/news/modify`;  //修改新闻
urlUtils.NewsDelete = `${baseURL}/sys/news/delete/`;  //删除新闻
urlUtils.NewsDetail = `${baseURL}/sys/news/detail`;  //查看新闻详情
urlUtils.NewsPublish = `${baseURL}/sys/news/publish`;  //发布新闻
urlUtils.NewsSubmit = `${baseURL}/sys/news/submit`;  //发布新闻
urlUtils.NewsReject = `${baseURL}/sys/news/reject`;  //驳回新闻
urlUtils.NewsReadAmount = `${baseURL}/sys/news/readAmount`;  //新闻阅读量

//拍卖公告管理
urlUtils.GetNoticeListByPage = `${baseURL}/sys/notice/list`;  // 分页查询拍卖公告列表
urlUtils.NoticeAdd = `${baseURL}/sys/notice/add`;  //新增拍卖公告
urlUtils.NoticeModify = `${baseURL}/sys/notice/modify`;  //修改拍卖公告
urlUtils.NoticeDelete = `${baseURL}/sys/notice/delete/`;  //删除拍卖公告
urlUtils.NoticeDetail = `${baseURL}/sys/notice/detail`;  //查看拍卖公告详情
urlUtils.NoticePublish = `${baseURL}/sys/notice/publish`;  //发布拍卖公告
urlUtils.NoticeReject = `${baseURL}/sys/notice/reject`;  //驳回拍卖公告
urlUtils.NoticeRelation = `${baseURL}/sys/notice/relation`;  //关联拍卖会
urlUtils.NoticeCancelRelation = `${baseURL}/sys/notice/cancelRelation`;  //取消关联拍卖会


//拍卖会管理
urlUtils.GetMeetListByPage = `${baseURL}/sys/meet/list`;  // 分页查询拍卖会列表
urlUtils.MeetAdd = `${baseURL}/sys/meet/add`;  //新增拍卖会
urlUtils.MeetModify = `${baseURL}/sys/meet/modify`;  //修改拍卖会
urlUtils.MeetDelete = `${baseURL}/sys/meet/delete/`;  //删除拍卖会
urlUtils.MeetDetail = `${baseURL}/sys/meet/detail`;  //查看拍卖会详情
urlUtils.MeetAddLot = `${baseURL}/sys/meet/addLot`;  //新增拍品
urlUtils.MeetNoticesListTen = `${baseURL}/sys/meet/noticesListTen`;  //根据公告标题查公告

//标的管理
urlUtils.GetLotListByPage = `${baseURL}/sys/lot/list`;  // 分页查询标的列表
urlUtils.LotModify = `${baseURL}/sys/lot/modify`;  //修改标的
urlUtils.LotRemarks = `${baseURL}/sys/lot/remarks`;  //备注
urlUtils.LotDelete = `${baseURL}/sys/lot/delete/`;  //删除


//竞买人管理
urlUtils.GetBidderListByPage = `${baseURL}/sys/bidder/list`;  // 分页查询竞买人列表
urlUtils.MeetAddBidder = `${baseURL}/sys/bidder/add`;  // 新增竞买人


//注册用户信息管理
urlUtils.GetUsersInfoListByPage = `${baseURL}/sys/usersInfo/list`;  // 分页查询注册用户列表
urlUtils.UsersInfoAdminReset = `${baseURL}/sys/usersInfo/adminReset`;  // 管理员重置注册用户密码
urlUtils.UsersInfoDetail = `${baseURL}/sys/usersInfo/detail`;  // 注册用户详情

//图片上传
urlUtils.FileUpload = `${baseURL}/sys/upload`;  // 图片上传接口


export {
    urlUtils,
    baseURL
}
