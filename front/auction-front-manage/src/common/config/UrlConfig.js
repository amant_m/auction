
/**
 * 配置网络请求环境
 * @type {string}
 */

let env = process.env.VUE_APP_CURRENTMODE || 'DEV';
//console.log("当前环境变量："  , env);
// let env = 'DEV';

let GlobalConfig = {
    DEV: {
        env: 'DEV',
        // baseIp:'http://10.100.240.26:8080/jdcrp'
        baseIp:'/api'
    },
    UAT: {
        env: 'UAT',
        baseIp: 'http://10.100.240.26:8080/jdcrp',
    },
    PROD: {
        env: 'PROD',
        baseIp: 'http://10.100.240.26:8080/jdcrp',
    },
}


// 环境介绍说明
// DEV： development                 开发
// SIT： System Integrate Test       系统整合测试（内测）
// UAT： User Acceptance Test        用户验收测试
// PET： Performance Evaluation Test 性能评估测试（压测）
// SIM： simulation                  仿真
// Mir： Mirror                      镜像 同仿真
// ABT:  ABTest                      灰度
// PROD：production                  产品/正式/生产

let urlConfig = GlobalConfig[env];
//console.log(urlConfig)

export {
    urlConfig,
    env
}
