import store from '../store'
import Vue from 'vue'

export function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

export function getCookie(key) {
    var cook = {};
    var cookie = document.cookie.split("; ");
    //console.log(cookie)
    for (var i = 0; i < cookie.length; i++) {
        var arr = cookie[i].split("=");
        cook[arr[0]] = arr[1]
    }
    return cook[key]

}

//清除cookie
export function clearCookie(name) {
    var exp = new Date();
    exp.setTime(exp.getTime() - 1);
    var cval = getCookie(name);
    if (cval != null)
        document.cookie = name + "=" + cval + ";expires=" + exp.toGMTString();
}

// ===============================sessionStorage相关===================================
//添加 sessionStorage
export function setSessionItem(key, value) {
    const session = window.sessionStorage;
    if (session) {
        session.setItem(key, value);
    }
}

//获取 sessionStorage
export function getSessionItem(key) {
    return sessionStorage.getItem(key)
}

//删除 sessionStorage
export function removeSessionItem(key) {
    return sessionStorage.removeItem(key)
}


let commonUtils = {
    setSessionItem: function (key, value) {
        setSessionItem(key, value)
    },
    getSessionItem: function (key) {
        return getSessionItem(key)
    },
    removeSessionItem: function (key) {
        removeSessionItem(key)
    },
    setCookie: function (cname, cvalue, exdays) {
        return setCookie(cname, cvalue, exdays)
    },
    getCookie: function (key) {
        return getCookie(key)
    },
    clearCookie: function (name) {
        return clearCookie(name)
    },
    toDateTimeString: function(date) {
        return convertToString('yyyy-MM-dd hh:mm:ss', date);
    }
}
export {
    commonUtils
}