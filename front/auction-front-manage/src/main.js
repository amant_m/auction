import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';


import HttpUtils from './common/HttpUtils';
import { commonUtils } from './common/util';
import Pagination from './components/commoncomponents/pagination'

Vue.component('Pagination', Pagination)
//引入echarts
import echarts from 'echarts'

Vue.prototype.$echarts = echarts;
//引入阿里图标库
import './assets/iconfont/iconfont.css'

Vue.use(HttpUtils);
Vue.use(ElementUI);


Vue.prototype.$commonUtils = commonUtils;  // 将公用方法放进原型


//全局导航前置钩子
//new 创建一个空对象，this引用该对象，同时继承propoty原型的
router.beforeEach((to, from, next) => {
    let userCode = commonUtils.getSessionItem('userId');
    let breadcrumbList = store.getters.getBreadcrumbList;  // 面包屑数据
    if (to.meta.title) {
        document.title = to.meta.title
    }
    store.commit('setPageName', to.name); // 保存当前页面名称
    let pathnameArr = to.path.substr(1).split('/');
    store.commit('setCurrentTab', pathnameArr[0]);
    store.commit('setCurrentMenu', pathnameArr[1]);

    //校验是否登录
    if (to.name == 'login' || to.name == 'resetPassword' || to.name == '404' || to.name == 'welcome' || to.name == 'about') {
        next();
    } else {
        next();
        let flag = true;
        breadcrumbList.map((item) => {
            if (item.path == to.path) {
                flag = false
            }
        })
        if (flag) {
            breadcrumbList.push(to);
            store.commit('setBreadcrumbList', breadcrumbList);
        }
        if (userCode && userCode.length > 0) {
            next()
        } else {
            next({ path: "/login" });
        }
    }
})
new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
