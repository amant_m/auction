import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/login'
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/login'),
    meta: {
      title: '登陆'
    }
  },
  {
    path: '/user',
    name: 'user',
    component: () => import('../components/layout/baseLayout'),
    meta: {
      subMenu: true, //是否含有子菜单，是否在头部展示tab
      title: '用户',
      desc: ''
    },
    children: [
      {
        path: '/user',
        name: 'batch',
        component: () => import('../components/layout/emptyLayout'),
        meta: {
          title: '用户',
          desc: ''
        },
        children: [
          {
            path: '/sys/sysManage',
            name: 'sysManage',
            component: () => import('../views/sysUser/Manage'),
            meta: {
              title: '后台用户管理'
            }
          },
          {
            path: '/user/bidderManage',
            name: 'bidderManage',
            component: () => import('../views/user/bidderManage'),
            meta: {
              title: '竞买人管理'
            },
          },
          {
            path: '/user/userManage',
            name: 'userManage',
            component: () => import('../views/user/userManage'),
            meta: {
              title: '注册用户管理'
            }
          },
          {
            path: '/auction/notice',
            name: 'notice',
            component: () => import('../views/auction/noticeManage'),
            meta: {
              title: '拍卖公告管理'
            }
          },
          {
            path: '/auction/meet',
            name: 'meet',
            component: () => import('../views/auction/meetManage'),
            meta: {
              title: '拍卖会管理'
            }
          },
          {
            path: '/auction/lot',
            name: 'lot',
            component: () => import('../views/auction/lotManage'),
            meta: {
              title: '标的管理'
            }
          },
          {
            path: '/news/newsManage',
            name: 'newsManage',
            component: () => import('../views/news/newsManage'),
            meta: {
              title: '新闻资讯'
            }
          },
          {
            path: '/news/newsReview',
            name: 'newsReview',
            component: () => import('../views/news/newsReview'),
            meta: {
              title: '新闻审核'
            }
          }
        ]
      }
    ]
  },
]

const router = new VueRouter({
  routes
})

export default router
