export const menuData = [
    {
        path: '/auction',
        name: 'auction',
        title: '拍卖管理',
        desc: 'language.menu.sysNot',
        children: [
            {
                path: '/auction/notice',
                name: 'notice',
                title: '拍卖公告管理',
                desc: '',
                children: []
            },{
                path: '/auction/meet',
                name: 'meet',
                title: '拍卖会管理',
                desc: '',
                children: []
            },{
                path: '/auction/lot',
                name: 'lot',
                title: '标的管理',
                desc: '',
                children: []
            }
        ]
    },
    {
        path: '/sys',
        name: 'sysManage',
        title: '后台用户管理',
        desc: 'language.menu.sysNot',
        children: [
            {
                path: '/sys/sysManage',
                name: 'sysManage',
                title: '用户管理',
                desc: '',
                children: []
            },
        ]
    },
    {
        path: '/user',
        name: 'manage',
        title: '前台用户管理',
        desc: 'language.menu.sysNot',
        children: [
            {
                path: '/user/bidderManage',
                name: 'bidderManage',
                title: '竞买人管理',
                desc: '',
                children: []
            }, {
                path: '/user/userManage',
                name: 'userManage',
                title: '注册用户管理',
                desc: '',
                children: []
            },
        ]
    },
    {
        path: '/news',
        name: 'news',
        title: '新闻管理',
        desc: 'language.menu.sysNot',
        children: [
            {
                path: '/news/newsManage',
                name: 'newsManage',
                title: '新闻资讯',
                desc: '',
                children: []
            },
            {
                path: '/news/newsReview',
                name: 'newsReview',
                title: '新闻审核',
                desc: '',
                children: []
            },
        ]
    }
    
]
