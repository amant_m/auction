/*
 * @Author: your name
 * @Date: 2020-03-23 19:28:32
 * @LastEditTime: 2020-04-21 10:54:34
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: \etrain-webpc\vue.config.js
 */
// const CompressionPlugin = require('compression-webpack-plugin')
// const Timestamp = new Date().getTime();
module.exports = {
    lintOnSave: false,  // 是否开启eslint
    publicPath: './',    // 打包文件相对路径地址
    outputDir: 'east',  // 打包后文件夹名称
    devServer: {
        open: true,//是否自动弹出
        port: 8091,//端口号
        https: false,
        hotOnly: true,//热更新
        proxy: {
            '/api': {
                target: 'http://localhost:8081/',//目标地址
                // ws: true, // 是否启用websockets
                secure: false, // 使用的是http协议则设置为false，https协议则设置为true
                changeOrigin: true, //开启代理：在本地会创建一个虚拟服务端，然后发送请求的数据，并同时接收请求的数据，这样服务端和服务端进行数据的交互就不会有跨域问题
                pathRewrite: { '^/api': '' }    //这里重写路径
            }
        }

    },

    //文件名加上hash  
    chainWebpack: config => {
        config.output.filename('[name].[hash:6].js').end();
        config.output.chunkFilename('[name].[hash:6].js').end();
    },
    //文件名加上时间戳 
    // chainWebpack: config => {
    //     config.output.filename(`[name].${Timestamp}.js`).end();
    //     config.output.chunkFilename(`[name].${Timestamp}.js`).end();
    // },
    // //开启gzip压缩
    // configureWebpack: {
    //     // 使用gzip压缩
    //     plugins: [
    //         new CompressionPlugin({//gzip压缩配置
    //             filename: '[path].gz[query]',
    //             algorithm: 'gzip',
    //             test:/\.js$|\.html$|\.css/,//匹配文件名
    //             threshold:10240,//对超过10kb的数据进行压缩
    //             minRatio: 0.8,
    //             deleteOriginalAssets:false,//是否删除原文件
    //         })
    //     ],
    //     // 文件名加上时间戳，避免cdn缓存
    //     output: { // 输出重构  打包编译后的 文件名称  【模块名称+时间戳】
    //         filename: `[name].${Timestamp}.js`,
    //         chunkFilename: `[name].${Timestamp}.js`
    //     }
    // },
}