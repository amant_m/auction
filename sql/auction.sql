-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: 123.56.22.60    Database: auction
-- ------------------------------------------------------
-- Server version	5.6.49-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bid_deal`
--

DROP TABLE IF EXISTS `bid_deal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bid_deal` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bidder_id` bigint(20) DEFAULT NULL,
  `meet_id` bigint(20) DEFAULT NULL,
  `lot_id` bigint(20) DEFAULT NULL,
  `deal_price` decimal(19,2) DEFAULT NULL COMMENT '成交价',
  `is_pay` int(2) DEFAULT NULL COMMENT '是否付款',
  `create_time` datetime DEFAULT NULL,
  `lot_name` varchar(45) DEFAULT NULL,
  `bidder_num` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='竞拍成功表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bid_deal`
--

LOCK TABLES `bid_deal` WRITE;
/*!40000 ALTER TABLE `bid_deal` DISABLE KEYS */;
INSERT INTO `bid_deal` VALUES (1,1,1,7,120000.00,1,'2021-05-05 17:04:09','北京房子','R1001');
/*!40000 ALTER TABLE `bid_deal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bidder`
--

DROP TABLE IF EXISTS `bidder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bidder` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bidder_id` bigint(20) DEFAULT NULL COMMENT '注册用户竞买人id',
  `bidder_num` varchar(10) DEFAULT NULL,
  `meet_id` bigint(20) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='竞买人信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bidder`
--

LOCK TABLES `bidder` WRITE;
/*!40000 ALTER TABLE `bidder` DISABLE KEYS */;
INSERT INTO `bidder` VALUES (1,6,'R1001',1,'2021-05-01 16:17:04'),(2,1,'R3291',1,'2021-05-01 16:17:04'),(3,6,'T1240',8,'2021-05-04 14:55:14'),(4,6,'Q2223',12,'2021-05-04 13:06:41'),(5,1,'M0821',7,'2021-05-04 13:06:41'),(8,1,'X7292',13,'2021-05-15 22:59:58'),(9,7,'S0169',10,'2021-05-20 14:06:10');
/*!40000 ALTER TABLE `bidder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lot`
--

DROP TABLE IF EXISTS `lot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lot` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL COMMENT '标的名称',
  `meet_id` bigint(20) DEFAULT NULL COMMENT '拍卖会id',
  `start_price` decimal(19,2) DEFAULT NULL COMMENT '起拍价，总长19位包括两位小数点',
  `assess_price` decimal(19,2) DEFAULT NULL,
  `increase_price` decimal(19,2) DEFAULT NULL COMMENT '加价幅度',
  `content` varchar(1000) DEFAULT NULL COMMENT '标的介绍',
  `auction_status` int(11) DEFAULT NULL COMMENT '拍卖状态',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `images` varchar(1000) DEFAULT NULL COMMENT '标的图片',
  `order_sort` int(10) DEFAULT NULL COMMENT '展示顺序',
  `remarks` varchar(100) DEFAULT NULL COMMENT '备注',
  `deal_price` decimal(19,2) DEFAULT NULL COMMENT '成交价',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COMMENT='标的信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lot`
--

LOCK TABLES `lot` WRITE;
/*!40000 ALTER TABLE `lot` DISABLE KEYS */;
INSERT INTO `lot` VALUES (1,'失败1',12,1000.00,50.00,2.00,'呼呼呼呼呼呼',0,'2021-03-30 17:42:56','2021-03-31 14:01:01','5f2b3f914f7a4a2d99dd9ab2914a1460.jpg',1,'55',NULL),(2,'失败2',1,10.00,50.00,2.00,'呼呼呼呼呼呼',0,'2021-03-30 17:42:56',NULL,'5f2b3f914f7a4a2d99dd9ab2914a1460.jpg',2,NULL,NULL),(3,'失败3',5,10.00,50.00,2.00,'呼呼呼呼呼呼',0,'2021-03-30 17:42:56',NULL,'5f2b3f914f7a4a2d99dd9ab2914a1460.jpg',3,NULL,NULL),(4,'第一次测试标的',11,10.00,50.00,2.00,'呼呼呼呼呼呼',0,'2021-03-30 17:42:56',NULL,'5f2b3f914f7a4a2d99dd9ab2914a1460.jpg',1,NULL,NULL),(5,'测试标的123',5,1.00,100.00,10.00,'介绍介毒贩夫妇付付付付付付付付付付绍介绍介绍介绍介绍介绍介绍介绍介绍介绍',0,'2021-05-02 15:49:56','2021-05-02 19:53:46','5c82fe7693e740f1baf5a6689174f713.jpg',1,NULL,NULL),(6,'北京房产',8,100000.00,200000.00,10000.00,'北京房产，精选第一',0,'2021-05-02 16:00:11','2021-05-02 20:28:19','59de1bce487543e192cc7b67a5cd2f74.png',1,NULL,NULL),(7,'北京房子',1,100000.00,1000000.00,10000.00,'标的介绍嘻嘻嘻嘻',1,'2021-05-02 16:02:48','2021-05-05 17:04:09','fa9feb90a3a64f36aa7ceae90ef75d83.jpg',1,'对的',120000.00),(9,'测试标的',7,1000.00,2000.00,10.00,'测试标的1',0,'2021-05-03 20:10:01',NULL,'ec75261f80684b14855f7aa21b094056.jpg',1,NULL,NULL),(10,'测试标的1',10,1.00,22.00,1.00,'大大大大大大的多多',0,'2021-05-08 21:25:11',NULL,'d623faac268347b8860f2dd7afdc3de2.png',2,NULL,NULL),(11,'测试标的2',7,2.00,2222.00,22.00,'介绍介绍介绍介绍介绍介绍介绍介绍介绍介绍介绍介绍介绍介绍介绍介绍介绍介绍介绍介绍介绍介绍介绍',0,'2021-05-08 21:25:44',NULL,'40a92935f79943129e2f6740e9a7b934.png',3,NULL,NULL),(12,'515拍卖会测试标的1',13,100.00,1000.00,20.00,'515拍卖会测试标的1',0,'2021-05-15 22:11:55','2021-05-15 22:31:39','af9c38327f5b443c9b726ed3a14e7f99.jpg',1,NULL,NULL),(13,'515拍卖会测试标的2',13,200.00,2000.00,30.00,'515拍卖会测试标的2',0,'2021-05-15 22:12:18','2021-05-15 22:31:27','bcc6d705ea404ecba45141d51168ae83.jpg',2,NULL,NULL),(14,'515拍卖会测试标的3',13,3000.00,30000.00,40.00,'515拍卖会测试标的3',0,'2021-05-15 22:12:45',NULL,'9ac50a970c5d4032a5fe9b049c86005e.png',3,NULL,NULL),(15,'515拍卖会测试标的4',13,5000.00,100000.00,50.00,'515拍卖会测试标的4',0,'2021-05-15 22:13:09',NULL,'861e955aa6db4ff6b0f0883b7a6fea42.png',4,NULL,NULL),(16,'515拍卖会测试标的5',13,6000.00,666666.00,60.00,'515拍卖会测试标的5',0,'2021-05-15 22:13:44','2021-05-15 22:26:19','66c25844650f47079e4e658b99f38319.jpg',5,NULL,NULL),(17,'515拍卖会测试标的6',13,22.00,333.00,1.00,'515拍卖会测试标的6',0,'2021-05-15 22:14:13','2021-05-15 22:24:27','980da9a00a6a43c3bb07e67834889f7c.jpg',6,NULL,NULL);
/*!40000 ALTER TABLE `lot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meet`
--

DROP TABLE IF EXISTS `meet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `meet` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `notice_id` bigint(20) DEFAULT NULL,
  `notice_title` varchar(100) DEFAULT NULL,
  `logo` varchar(150) DEFAULT NULL COMMENT '拍卖会封面',
  `meet_state` int(11) DEFAULT NULL COMMENT '拍卖会状态 1正在进行 2 即将开始3已结束',
  `publish_state` int(11) DEFAULT NULL COMMENT '发布状态 0未发布 1已发布 2已撤销',
  `link_man` varchar(45) DEFAULT NULL COMMENT '联系人',
  `link_phone` varchar(45) DEFAULT NULL COMMENT '联系方式',
  `lot_count` int(11) DEFAULT NULL COMMENT '标的总数',
  `start_time` datetime DEFAULT NULL COMMENT '拍卖会开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '拍卖会结束时间',
  `create_time` datetime DEFAULT NULL,
  `publish_by` varchar(45) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `publish_time` datetime DEFAULT NULL COMMENT '发布时间',
  `deadline_time` datetime DEFAULT NULL COMMENT '报名截止时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COMMENT='拍卖会信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meet`
--

LOCK TABLES `meet` WRITE;
/*!40000 ALTER TABLE `meet` DISABLE KEYS */;
INSERT INTO `meet` VALUES (1,'321的拍卖会',321,'拍卖公告第一次','5f2b3f914f7a4a2d99dd9ab2914a1460.jpg',3,1,'海滨测试小队','13343257498',3,'2021-05-01 15:15:00','2021-05-05 16:15:00',NULL,'海滨测试公司1','2021-04-28 09:49:37','2021-04-28 09:49:37','2021-05-01 15:00:00'),(2,'1号拍卖会',42,'测试公告_0708下发的公告0708（变卖）','53400eeb4a374dda91851ff79d590f26.jpg',3,1,'海滨测试小队','13343257498',1,'2021-07-26 00:00:00','2021-04-28 10:38:55',NULL,'海滨测试公司1','2021-05-05 15:55:46','2021-05-05 15:55:46','2021-04-28 10:38:57'),(3,'测试新增拍卖会',326,'测试第一次新增公告','43cb5297e1a54bf68fc3b79adf79c5f0.jpg',3,1,'海滨测试小队','13343271940',NULL,'2021-04-13 02:02:00','2021-04-14 01:01:01','2021-04-18 16:00:45','海滨测试公司1','2021-04-18 21:16:44','2021-04-18 21:16:44','2021-04-13 02:00:00'),(4,'测试拍卖会22223',48,'测试公告_1020下发的拍卖公告07090101','3e8eedb039b04d648eaa8c63cb0fcf31.jpg',3,0,'123','123',NULL,'2021-04-27 00:00:00','2021-04-13 00:00:00','2021-04-18 16:20:37','海滨测试公司1',NULL,NULL,'2021-04-29 00:00:00'),(5,'测试税务的拍卖会',15,'测试税务','9ea32b46c0914f33829f48f178e95d6c.jpg',3,1,'海滨联系人','13343271940',NULL,'2021-04-19 00:00:00','2021-04-27 00:00:00','2021-04-18 18:15:45','海滨测试公司1','2021-04-25 14:33:47','2021-04-25 14:33:47','2021-04-18 00:00:00'),(7,'测试拍卖会0508',43,'测试公告_1020下发的拍卖公告07080101','2c6ae103b1c541e08e6ec7fbaac28734.jpg',3,1,'123','123',NULL,'2021-05-08 00:00:00','2021-05-11 00:00:00','2021-05-01 13:01:54','海滨测试公司1','2021-05-03 20:09:17','2021-05-03 20:09:17','2021-05-07 00:00:00'),(8,'测试拍卖会0509',3,'良心公告','fa9feb90a3a64f36aa7ceae90ef75d83.jpg',3,1,'海滨测试公司1','13343271940',NULL,'2021-05-05 12:00:00','2021-05-05 13:00:00','2021-05-01 13:22:01','海滨测试公司1','2021-05-01 16:52:56','2021-05-01 16:52:56','2021-05-04 11:00:00'),(9,'测试拍卖会201907030001',36,'测试公告201907030001','54aeddc01f69468283a3e8c6ab9e4915.jpg',3,1,'123','123',NULL,'2021-05-03 11:42:49','2021-05-04 00:00:00','2021-05-02 11:43:38','海滨测试公司1','2021-05-05 15:56:11','2021-05-05 15:56:11','2021-05-01 00:00:00'),(10,'测试公告_1020下发的变卖拍卖会07040002',40,'测试公告_1020下发的变卖公告07040002','1db54529bf084c54844ea88b2591ec95.png',1,1,'海滨测试大队','13343271940',NULL,'2021-05-22 11:47:55','2021-05-25 00:00:00','2021-05-02 11:48:05','海滨测试公司1','2021-05-20 09:50:15','2021-05-20 09:50:15','2021-05-21 00:00:00'),(11,'测试拍卖会0709',47,'测试公告_0709下发的公告0709','15b3cec1cb044023a0d6d7c4e04e0a38.jpg',3,1,'123','123',NULL,'2021-05-04 00:00:00','2021-05-05 00:00:00','2021-05-02 11:49:33','海滨测试公司1','2021-05-03 20:07:42','2021-05-03 20:07:42','2021-05-02 00:00:00'),(12,'测试拍卖会0708',41,'测试公告_0708下发的公告0708','72c6fa6840444600a80459874a269e50.jpg',3,1,'123','123',NULL,'2021-05-05 00:00:00','2021-05-05 16:19:00','2021-05-02 11:56:22','海滨测试公司1','2021-05-05 15:56:05','2021-05-05 15:56:05','2021-05-05 00:00:00'),(13,'测试拍卖会515',39,'测试公告_1020下发的拍卖公告07040001','fd51a76fb7e74de1a24a1c3b5228cba3.png',2,1,'海滨测试公司','13343271940',NULL,'2021-05-15 02:02:02','2021-05-21 03:02:02','2021-05-02 11:59:01','海滨测试公司1','2021-05-15 22:15:00','2021-05-15 22:15:00','2021-05-13 00:58:56'),(14,'测试拍卖会1456',34,'测试公告_0625下发的公告0625','88fc36422d954988b11f397573c33147.jpg',1,0,'123','123',NULL,'2021-05-04 00:00:00','2021-05-05 00:00:00','2021-05-02 12:02:25','海滨测试公司1',NULL,NULL,'2021-05-02 12:02:14'),(15,'测试拍描绘121',22,'0621测试司法ftp','aa7c220102674a30b0299651e216d320.jpg',1,0,'123','123',NULL,'2021-05-04 00:00:00','2021-05-05 00:00:00','2021-05-02 12:03:07','海滨测试公司1',NULL,NULL,'2021-05-02 12:03:02'),(16,'测试拍卖会大捷',30,'0625司法测试20199','7305c45a7613483583903721a2ef1aa5.jpg',1,0,'123','123',NULL,'2021-05-04 00:00:00','2021-05-05 00:00:00','2021-05-02 12:07:18','海滨测试公司1',NULL,NULL,'2021-05-02 12:07:13'),(17,'测试公告_1020下发的拍卖会9110001',14,'测试房产其他房产公告','00e52d8990584784a02cd5dfa5f3db6d.jpg',1,0,'13343271940','海滨测试大队',NULL,'2021-05-04 00:00:00','2021-05-05 00:00:00','2021-05-02 12:09:19','海滨测试公司1','2021-05-02 14:52:05',NULL,'2021-05-02 12:09:13'),(18,'测试房产其他房产拍卖会',14,'测试房产其他房产公告','f74b534bc0504673b63fec53e62a8313.jpg',1,0,'海滨测试大队','13343271940',NULL,'2021-05-04 00:00:00','2021-05-05 00:00:00','2021-05-02 12:11:22','海滨测试公司1','2021-05-02 19:50:51',NULL,'2021-05-02 12:11:17'),(19,'测试拍卖会456',35,'0625测试司法ftp0625','7fc25cac276845ee83b4baeaaf446d1f.jpg',1,0,'456','456',NULL,'2021-05-06 00:00:00','2021-05-07 00:00:00','2021-05-02 13:12:43','海滨测试公司1','2021-05-02 13:30:12',NULL,'2021-05-03 13:12:13');
/*!40000 ALTER TABLE `meet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `news` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `module` int(11) DEFAULT NULL COMMENT '模块 1最新通知 2最新资讯 3知识贴士',
  `content` text,
  `reject` varchar(100) DEFAULT NULL COMMENT '驳回原因',
  `state` int(11) DEFAULT NULL COMMENT '发布状态 0未发布 1已发布 2已撤销',
  `read_amount` bigint(20) DEFAULT NULL COMMENT '浏览人数',
  `publish_by` varchar(45) DEFAULT NULL COMMENT '发布人',
  `publish_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='新闻管理';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (1,'地第一次新增',1,'哈哈哈哈哈哈红红火火恍恍惚惚','测试驳回',1,10,'管理员','2021-04-21 17:19:02',NULL,'2021-03-25 00:00:00'),(4,'地第si次新增',1,'hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh','测试不同按钮相同接口',3,1,'管哈哈哈哈',NULL,'2021-05-10 20:05:07','2021-03-26 00:00:00'),(5,'地第10次新增',2,'hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh','测试审核',3,8,'管理员',NULL,'2021-04-25 11:37:35','2021-03-26 00:00:00'),(6,'前端测试提交新闻',2,'哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈hh',NULL,1,10,'海滨测试小队好','2021-04-21 17:18:58','2021-04-16 00:00:00','2021-04-15 00:00:00'),(8,'测试删除用的新闻啊',2,'由中国信达陕西分公司主办、中拍平台承办的“资产推介 商机共享”直播活动于3月31日落下帷幕。虽然只有1个小时的直播推介，但吸引了不少意向买家关注和驻足。本次资产推介活动是中拍平台联合资产公司举办的首次线上直播活动，其一，创新了资产推介会的传统形式，将线下推介会转战线上，大家足不出户就可以尽享资产信息；其二，创新拍卖模式，拍卖融合直播新潮流，为接下来的“拍卖+直播”模式做好准备！\n\n \n\n\n\n \n\n网络拍卖助力资产处置\n\n \n\n网络拍卖实现了全程、全面和全网络公开，通过网上拍卖模式的创新、全面提高了资产处置的效率。眼下，中拍平台联合全国各地拍卖企业，打造金融不良资产线上、线下交易与服务体系，建立系统+服务的综合模式，为金融机构处置债权、抵债资产。2021年“中拍·金融资产”板块上线运营，引来不少金融机构的关注和使用，为中国信达、中国长城资产、浦发银行等打造了金融资产版块的专项入口，方便竞买人查看优质资产信息。\n\n \n\n\n\n \n\n“资产+直播”、“拍卖+直播”\n\n \n\n监管数据显示，截至 2020 年 6月末，商业银行不良贷款余额和不良贷款率总体较年初有所上升，其中，不良贷款余额较年初增加 4004 亿元，不良贷款率比年初上升 0.08 个百分点。在此情况下，一些银行和资产管理公司也开始更积极地为这些资产寻找接手方。于是，面向更多用户的互联网直播平台进入了他们的视线。\n\n \n\n\n\n \n\n而拍卖行业作为服务性行业，拥有14000余名专业拍卖师人才储备！眼下，中拍平台联合资产公司助力不良资产直播推介。接下来，中拍平台会联合拍卖企业，帮助资产方策划更多直播活动，开发直播拍卖产品，让拍卖更有趣！此次直播活动告一段落，资产招商还在火热进行，有任何资产需求记得联系中拍平台！ ',NULL,1,29,'管理员阿芒','2021-04-16 00:00:00',NULL,'2021-04-16 00:00:00'),(12,'回顾疫情下拍卖市场',2,'多年以后，回望拍卖历史，2019年无疑是值得永远铭记的一年。\n\n这一年里挑战与机遇并存。\n\n挑战是，中美贸易战导致的关税加重，使艺术行业遭遇巨大压力和危机；拍卖行再遇巨大人事变动，先是佳士得亚洲区主席魏蔚2020年离职，接着是蘇富比私有化、业务架构重组，二级市场2020年将会有怎样的战略调整……\n\n机遇是，2019年似乎成为了“潮流艺术”的拍卖年，KAWS、奈良美智、草间弥生、班克斯等刷新其作品拍卖纪录；阔别拍场三十余年的莫奈1850年作《干草堆》1.107亿美元（折合人民币7.6亿元）成交，委托人获得43倍的回报，不仅创造了莫奈作品拍卖的新纪录，也是其作品首次跻身1亿美元俱乐部，亦成为2019年全球最贵拍品；时隔仅六个月，杰夫·昆斯超越大卫·霍克尼《艺术家肖像（泳池与两个人像）》，他又一次成为身价最高的在世艺术家；常玉作品首次破三亿港币成交，刷新作品拍卖纪录……',NULL,2,2,'海滨拍卖小队6',NULL,'2021-04-25 13:50:25','2021-04-21 17:21:18'),(13,'2021.4.25最新新闻资讯——海滨读书日',2,'   海滨读书日 https://mp.weixin.qq.com/s/BB7J_vrkNKwD6vcJHLCKwA\n\n\n 这是你在学校的第几个读书日？\n\n 如果学校是一本书\n\n生活成了惊艳读者的文字\n\n点点滴滴都成了故事中的起承转合\n\n让我们用整个青春去读吧\n\n\n','哈哈哈哈哈哈哈',3,8,'海滨大队',NULL,'2021-05-20 15:33:15','2021-04-25 11:45:45');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notice`
--

DROP TABLE IF EXISTS `notice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `notice` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `meet_id` bigint(20) DEFAULT NULL COMMENT '拍卖会id',
  `meet_name` varchar(100) DEFAULT NULL COMMENT '拍卖会名称',
  `title` varchar(100) NOT NULL COMMENT '公告标题',
  `notice_code` varchar(100) NOT NULL COMMENT '公告32位唯一编号',
  `state` int(11) DEFAULT NULL COMMENT '公告状态：0未发布， 1已发布， 2已撤回',
  `content` longtext COMMENT '公告内容',
  `reject` varchar(500) DEFAULT NULL COMMENT '撤回原因',
  `publish_by` varchar(50) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `publish_time` datetime DEFAULT NULL COMMENT '公告发布时间',
  `short_describe` varchar(100) DEFAULT NULL COMMENT '简介',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=328 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='拍卖公告信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notice`
--

LOCK TABLES `notice` WRITE;
/*!40000 ALTER TABLE `notice` DISABLE KEYS */;
INSERT INTO `notice` VALUES (1,2,'1号拍卖会','测试公告0724001','A9CDF1C97C744B5CAD8F302E8DC3B819',2,NULL,'测试拍卖会是否撤回','海滨测试公司1','2018-07-24 10:10:39','2021-04-28 10:16:31',NULL,NULL),(3,8,'良心拍卖会','良心公告','0B5BBEEBBF814D24981344F3189FFD2E',1,NULL,NULL,'海滨测试公司1','2018-07-30 10:44:28','2021-05-01 16:52:56','2021-05-01 16:52:56','测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告'),(4,NULL,NULL,'跑车一秀','A3B14AAE70BA47858F5116834B376165',0,NULL,'测试发布时间是否会为空','海滨测试公司1','2018-07-30 11:05:38','2021-03-28 17:20:36',NULL,NULL),(5,NULL,NULL,'下发的1010公告01040001','00000000000000000001201807300001',0,NULL,'测试发布时间是否会为空','海滨测试公司1','2018-07-30 15:19:08','2021-03-28 17:29:12',NULL,NULL),(6,NULL,NULL,'测试公告_1020下发的拍卖公告07300001','00000000000000000000201807300001',0,NULL,'测试发布时间是否会为空','海滨测试公司1','2018-07-30 15:28:19','2021-03-28 17:51:25',NULL,NULL),(9,NULL,NULL,'测试公告_1020下发的拍卖公告07310008','00000000000000000000201808010018',0,NULL,'敌人太强大，果断撤退','海滨测试公司1','2018-08-01 10:39:05','2018-07-24 10:10:39',NULL,NULL),(10,NULL,NULL,'下发的1010公告08010001','00000000000000000000201808010008',0,NULL,NULL,'海滨测试公司1','2018-08-01 16:38:12','2018-07-24 10:10:39',NULL,NULL),(11,NULL,NULL,'测试公告_1020下发的拍卖公告08010002','00000000000000000000201808010002',0,NULL,NULL,'海滨测试公司1','2018-08-01 18:48:02','2018-07-24 10:10:39',NULL,NULL),(12,NULL,NULL,'测试公告_1020下发的拍卖公告08150001','00000000000000000000201808150001',0,NULL,NULL,'海滨测试公司1','2018-08-15 10:51:03','2018-07-24 10:10:39',NULL,NULL),(13,NULL,NULL,'测试测试数据公告','C3004E6F2DF04D85925932D0E7B8C139',0,NULL,NULL,'海滨测试公司1','2018-09-06 10:30:45','2018-07-24 10:10:39',NULL,NULL),(14,NULL,NULL,'测试房产其他房产公告','332164D9721848E6BD34DC04AB4A2C2F',0,NULL,NULL,'海滨测试公司1','2018-09-06 10:38:02','2018-07-24 10:10:39',NULL,NULL),(15,5,'测试税务的拍卖会','测试税务','30B98C109AAC4A829330032029DBEBE6',1,NULL,NULL,'海滨测试公司1','2018-09-06 10:58:56','2021-04-25 14:33:47','2021-04-25 14:33:47','测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告'),(16,NULL,NULL,'测试新版类型','B29E7ADAE4204D828B08FDC2643C5EFB',0,NULL,NULL,'海滨测试公司1','2018-09-06 14:41:06','2018-07-24 10:10:39',NULL,NULL),(18,NULL,NULL,'测试公告_1020下发的拍卖公告09110001','00000000000000000000201809110001',0,NULL,NULL,'海滨测试公司1','2018-09-11 14:06:09','2018-07-24 10:10:39',NULL,NULL),(19,NULL,NULL,'1','28CE8AD9E0E346A3914481F6AB4F32BE',0,NULL,NULL,'海滨测试公司1','2019-05-20 01:56:00','2018-07-24 10:10:39',NULL,NULL),(20,NULL,NULL,'合肥市瑶海区人民法院关于日立挖掘机(机型：日立ZX210LC-3型，机号：AWTA101158)（第一次拍卖）的公告','5494DB94C15F7E9330EFCCD9191123FC',0,NULL,NULL,'海滨测试公司1','2019-05-28 15:36:54','2018-07-24 10:10:39',NULL,NULL),(22,NULL,NULL,'0621测试司法ftp','00000000000000000000201906210000',0,NULL,NULL,'海滨测试公司1','2019-06-21 14:25:11','2018-07-24 10:10:39',NULL,NULL),(30,NULL,NULL,'0625司法测试20199','780FBAA084D3402A94775CBDE7E7BE66',0,'测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试',NULL,'海滨测试公司1','2019-06-25 15:27:27','2021-05-17 16:25:13',NULL,'测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试'),(32,NULL,NULL,'拍卖公告测试1111','FC22F8AF20A84D408BE62B8561E29C74',0,'111111111111111111111111',NULL,'海滨测试公司1','2019-06-25 15:46:25','2021-05-17 16:22:33',NULL,'1111111111111111111'),(34,NULL,NULL,'测试公告_0625下发的公告0625','00000000000000000000201906250001',0,NULL,NULL,'海滨测试公司1','2019-06-25 16:23:01','2018-07-24 10:10:39',NULL,NULL),(35,19,'测试拍卖会456','0625测试司法ftp0625','00000000000000000000201906250000',0,NULL,NULL,'海滨测试公司1','2019-06-25 17:10:00','2018-07-24 10:10:39',NULL,NULL),(36,9,'测试拍卖会201907030001','测试公告201907030001','DC282EF898C94BE487850D3450A14B66',1,NULL,NULL,'海滨测试公司1','2019-07-03 14:41:35','2021-05-05 15:56:11','2021-05-05 15:56:11','测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告'),(39,13,'测试拍卖会070411','测试公告_1020下发的拍卖公告07040001','00000000000000000000201907040001',1,'测试公告_1020下发的拍卖公告07040001测试公告_1020下发的拍卖公告07040001测试公告_1020下发的拍卖公告07040001测试公告_1020下发的拍卖公告07040001测试公告_1020下发的拍卖公告07040001测试公告_1020下发的拍卖公告07040001测试公告_1020下发的拍卖公告07040001测试公告_1020下发的拍卖公告07040001测试公告_1020下发的拍卖公告07040001测试公告_1020下发的拍卖公告07040001测试公告_1020下发的拍卖公告07040001测试公告_1020下发的拍卖公告07040001测试公告_1020下发的拍卖公告07040001测试公告_1020下发的拍卖公告07040001测试公告_1020下发的拍卖公告07040001测试公告_1020下发的拍卖公告07040001测试公告_1020下发的拍卖公告07040001测试公告_1020下发的拍卖公告07040001测试公告_1020下发的拍卖公告07040001测试公告_1020下发的拍卖公告07040001测试公告_1020下发的拍卖公告07040001测试公告_1020下发的拍卖公告07040001测试公告_1020下发的拍卖公告07040001测试公告_1020下发的拍卖公告07040001测试公告_1020下发的拍卖公告07040001测试公告_1020下发的拍卖公告07040001测试公告_1020下发的拍卖公告07040001测试公告_1020下发的拍卖公告07040001测试公告_1020下发的拍卖公告07040001测试公告_1020下发的拍卖公告07040001测试公告_1020下发的拍卖公告07040001测试公告_1020下发的拍卖公告07040001','修改拍卖会','海滨测试公司1','2019-07-08 10:22:22','2021-05-15 22:15:00','2021-05-15 22:15:00','测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告'),(40,10,'测试公告_1020下发的变卖拍卖会07040002','测试公告_1020下发的变卖公告07040002','00000000000000000000201907040002',1,'测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告','修改','海滨测试公司1','2019-07-08 10:22:26','2021-05-20 09:50:15','2021-05-20 09:50:15','测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告'),(41,12,'测试拍卖会0708','测试公告_0708下发的公告0708','00000000000000000000201907080001',1,NULL,NULL,'海滨测试公司1','2019-07-08 14:51:01','2021-05-05 15:56:05','2021-05-05 15:56:05','测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告'),(42,2,'1号拍卖会','测试公告_0708下发的公告0708（变卖）','00000000000000000000201907080002',1,NULL,NULL,'海滨测试公司1','2019-07-08 14:57:01','2021-05-05 15:55:46','2021-05-05 15:55:46','测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告'),(43,7,'测试拍卖会大姐姐','测试公告_1020下发的拍卖公告07080101','00000000000000000000201907080101',1,NULL,'关联的拍卖会不正确','海滨测试公司1','2019-07-08 15:01:01','2021-05-03 20:09:18','2021-05-03 20:09:18','测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告'),(47,11,'测试拍卖会0709','测试公告_0709下发的公告0709','00000000000000000000201907090002',1,'去','测试刷新','海滨测试公司1','2019-07-09 16:42:03','2021-05-03 20:07:42','2021-05-03 20:07:42',' 去测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告测试公告'),(49,NULL,NULL,'测试公告_1020下发的变卖公告07090102','00000000000000000000201907090102',2,'1','测试关联拍卖会','海滨测试公司1','2019-07-09 17:30:02','2021-05-04 12:54:16',NULL,'5'),(321,1,'321的拍卖会','拍卖公告第一次','3F714D72EBC942A2A23727E1DF9A192A',1,'哈哈哈哈哈哈红红火火恍恍惚惚','驳回','管理员hhhhhh','2021-03-27 19:48:49','2021-04-28 09:49:38','2021-04-28 09:49:38','公告简介公告简介公告简介公告简介公告简介公告简介公告简介公告简介公告简介公告简介公告简介公告简介公告简介公告简介公告简介公告简介公告简介公告简介公告简介公告简介公告简介公告简介'),(326,3,'测试新增拍卖会','测试第一次新增公告','5C41653D1F224AAFBD646EA99731A869',1,'    测试第一次新增公告测试第一次新增公告测试第一次新增公告测试第一次新增公告测试第一次新增公告测试第一次新增公告测试第一次新增公告测试第一次新增公告测试第一次新增公告测试第一次新增公告测试第一次新增公告测试第一次新增公告测试第一次新增公告测试第一次新增公告测试第一次新增公告','测试','海滨测试小队哈哈哈','2021-04-17 14:23:17','2021-04-18 21:16:44','2021-04-18 21:16:44','测试第一次新增公告测试第一次新增公告测试第一次新增公告测试第一次新增公告测试第一次新增公告测试第一次新增公告测试第一次新增公告测试第一次新增公告测试第一次新增公告测试第一次新增公告测试第一次新增公告');
/*!40000 ALTER TABLE `notice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `real_name` varchar(45) NOT NULL COMMENT '真实姓名',
  `password` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `type` int(2) DEFAULT NULL,
  `per_describe` varchar(200) DEFAULT NULL COMMENT '描述',
  `create_time` date DEFAULT NULL,
  `update_time` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='用户信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Amant','王萌萌','53d1223856b077fcf05b8e09bf085073','13343271940',2,'密码是521623','2021-02-18','2021-04-13'),(3,'Zaria','王萌2','e10adc3949ba59abbe56e057f20f883e','13811543097',1,NULL,'2021-02-19',NULL),(9,'测试15','张三啊','e2fc714c4727ee9395f324cd2e7f331f','13343271940',1,NULL,'2021-03-24','2021-03-24'),(11,'zutto','zd','e10adc3949ba59abbe56e057f20f883e','13811543097',1,'1243','2021-04-10',NULL),(14,'zhangsan','张三','01d7f40760960e7bd9443513f22ab9af','12214568745',1,'密码是zhangsan','2021-04-13',NULL),(16,'Lisi','李四','c3cb6d12c40908943b64bc0681af47db','12254874596',1,'密码是lisi123','2021-04-15',NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_info`
--

DROP TABLE IF EXISTS `users_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) NOT NULL,
  `real_name` varchar(45) DEFAULT NULL,
  `phone` varchar(45) NOT NULL,
  `mail` varchar(100) DEFAULT NULL,
  `sex` int(2) DEFAULT NULL,
  `is_real` int(4) DEFAULT NULL COMMENT '是否实名',
  `id_card` varchar(100) DEFAULT NULL COMMENT '身份证',
  `id_back` varchar(100) DEFAULT NULL COMMENT '身份证反面',
  `id_front` varchar(100) DEFAULT NULL COMMENT '身份证正面',
  `is_admin_reset` int(2) DEFAULT NULL COMMENT '是否被管理员重置密码',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='平台用户信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_info`
--

LOCK TABLES `users_info` WRITE;
/*!40000 ALTER TABLE `users_info` DISABLE KEYS */;
INSERT INTO `users_info` VALUES (1,'Amant','53d1223856b077fcf05b8e09bf085073','张三','13343271940','64284741.qq.com',1,1,'111','ack.jpg','jah.jpg',0,'2021-04-01 09:20:29','2021-05-06 16:03:21'),(4,'13343271941','e10adc3949ba59abbe56e057f20f883e',NULL,'13343271941',NULL,NULL,0,'123',NULL,NULL,0,'2021-04-01 09:26:51',NULL),(5,'123','e10adc3949ba59abbe56e057f20f883e',NULL,'13811543098',NULL,NULL,0,NULL,NULL,NULL,0,'2021-05-01 16:11:26','2021-05-06 16:25:33'),(6,'zutto','827ccb0eea8a706c4c34a16891f84e7b','赵丹','12345678900','948782013@qq.com',0,1,'110105199703312929','7bc09d42c0964db98df85f3a2eb525bc.jpg','8256e61d22984c3dbea738f880255393.jpg',0,'2021-05-01 16:17:04','2021-05-06 10:43:30'),(7,'Wangmengmeng','de32e99bea57ff069970b070ad5c8a8d','王萌萌','133343271940','64284741@qq.com',0,1,'230704199706270447','2ee5c2ebac684bd4b44e4cba271f8e77.jpg','1a846431762b43a8a6155b995a18dddc.jpg',0,'2021-05-05 16:01:14','2021-05-05 16:05:07'),(8,'Zaria','53d1223856b077fcf05b8e09bf085073',NULL,'13343271942',NULL,NULL,0,NULL,NULL,NULL,0,'2021-05-15 23:09:40',NULL);
/*!40000 ALTER TABLE `users_info` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-09 15:56:33
